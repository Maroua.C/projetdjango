** Projet web
 Maroua CHAHDIL  **

## Lien vers le manuel d’utilisation de l’application
[Manuel d'utilisation ](https://drive.google.com/file/d/1vEcA8kKKzkxseqvE7LhGgjdCfCeoRzjC/view?usp=sharing)

# Environnement, package et outils  


## Installation de miniconda
Conda permet de gerer les packages. En effet, c'est un système de gestion d'environnement open source et multiplateforme. 

https://docs.conda.io/en/latest/miniconda.html

## Création d'un environnement Conda

`conda create --name djangoenv python=3.8`


### Activation l'enviroment

`conda activate djangoenv`

### Installation les packages

```
conda install --name djangoenv django==3.0.3
conda install --name djangoenv sqlparse==0.3.1
```


permet de gerer l'inscritpion :
`conda install django-crispy-forms `

### Verification que django est installer

`pip freeze`

# Démarches nécessaires pour la réalisation du projet 

## Les  imports utilisées dans le projet

**Le nom du projet : quizdjango**


```
cd ${quizdjango}

python manage.py migrate
python manage.py runserver
```


**Le nom de l'application : quizdjangoapp**

Nous avons lancer le projet on peut y acceder dans un navigateur via l'url: http://127.0.0.1:8000/home 


### URL import

urls importation basique et necessaire :
```
from django.contrib import admin
from django.urls import path
```

A chaque création d'une url on a besoin de la fonction dans views de ce faire importation se fait comme suit:
Par exemple :
from quizdjangoapp.views import function_foo 


### VIEW import
view importation importante :
```
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import redirect
```



## DATABASE import
```
from quizdjangoapp.models import anwsertable
from quizdjangoapp.models import questiontable
from quizdjangoapp.models import imagetable
```


-> faire les autre import ?



### Chargement des fichiers csv dans la base de donnée  
Utilisation du fichier *model.py*

**Création des tables** :Questions, Réponses et Images. 
Respectivement nommées: questiontable,anwsertable et imagetable

**Prenons l'exemple de questiontable (meme principe pour les deux autres) :**

**Utilisation de la ligne de commande :**
python manage.py dbshell


**Creation de la table directement dans sqlite3 **
```
BEGIN;
CREATE TABLE "quizdjangoapp_questiontable" (
"id" integer NOT NULL PRIMARY KEY,
"questionid" int  NOT NULL,
"question" varchar(255) NOT NULL,
"type" varchar(255) NOT NULL,
"imagefield" varchar(255) NOT NULL,
"point" int NOT NULL,
"n_answer" int NOT NULL,
"n_image" int NOT NULL

);
COMMIT;
```

Fermeture de dbshell

**Utilisation de la ligne de commande :**
`python manage.py shell`

**Utilisation du script contenu dans questiontable.py**

Creation de la table via un script executé :

```
exec(open('questiontable.py').read())
exit(0)
```


Fermeture de shell


**Remarques :** 

- Pour la table imagetable ajout d'une colonne pour stocker les chemins des images 

    - les images sont stockées dans le dossiers image dans :
        - /Users/marwa/PycharmProjects/quizdjango/quizdjangoapp/static/image


- La table usertable contient les informations de chaque utilisateur :

    - L'atribut name contient le nom de l'utilisateur.
    - L'atribut score permet de stocker le score de chaque utilisateur.

- Utilisation de  django.contrib.auth.models pour utiliser la table User
    -  `from django.contrib.auth.models import User`


# Page de presentation 

-> Utilisation : Boostrap (bootstrap.min.css, bootstrap.min.js ), intallation de   boostrap ou bien     utilisation de l'url disponible dans leur site directement (bootstrap sera utiliser dans toute les page html majoritairement pour la header qui va permetre de naviguer entre les pages)

                
Page de présentation du site  *welcom.html*

L'image est générer aléatoirement a chaque actualisation de la page.Elle vient de la base de donnée directement.

**Cette page contient des liens pour accéder**

- **Quiz** :
    - partie component
    - partie microscopy


**Explorer les donnée** 

**Navigation**
- Dans la bare du haut  : 

    - Inscription
    - Connection

-> tout cela sera detaillés en aval.

# Quiz section

### View
**Importation necessaire :**
`import random`

**Fonction :** 
- quiz

- Creation de la fonction quiz qui contient la mise en place du quiz.
- Fonction quiz possede un parametre question_type qui definit le type de question, soit component ou microscopy.


- saveans
    - Fonction permettant de sauvegarder la reponse de l'utilisateur. 


- result
    - Fonction permettant d'afficher le resultat dans une page html spécifique.


### Url
**Appel du quiz via l'url contenu dans **

```
urlpatterns = [
    ...
    path('quiz/<question_type>', quiz),
    path('saveans/', saveans),
    path('result/<question_type>', result),


]
```


### Template
Utilisation de css et js.

*quiz.html* 
Permet d'afficher la question et les réponses avec les images génerer aléatoirement.

Le nombre d’images et de réponses est générer en fonction de ce qui est indiquer dans la table questionnable.

*result.html* 
Affiche le score a l'utilisateur.


# Explore section

### View
**importation necessaire :**

```
from django.shortcuts import redirect
from .form import Formimagetable #Form puis mettre Majuscule premiere lettre sinon ca ne marche pas
```

**Fonction :**

- explore
    - Fonction qui permet de gerer la page d'exploration


- listdata
    - Fonction qui va afficher les informations en liens avec la table imagetable de la base de donnée

- formsearch and formresult fonction sont utilisées pour la partie recherche. 

### Url
L'explore page a besoin des urls suivante 

```
urlpatterns = [
    ...
    path('listData/',listData),
    path('explore/',explore),

    path('formsearch/', formsearch),
    path('formresult/<str:mode>/', formresult),


]
```


Pour simuler un formulaire de recherche on va crée deux urls : formsearch et formresult


### Forms
*Forms.py* 

**Importation necessaire:**
> from .models import imagetable


Permet de crée le formulaire de recherche via la classe **Formimagetable** on va utilise la table imagetable issu de la base de donnée

**Remarque :**
Registerform et Formusertable seront abordé plus tard etant donnée que c'est pour l'inscriptionet la connection de l'utilisateur.


### Template
**Jquery   (jquery-3.4.1)**, intallation de   Jquery ou bien utilisation de l'url disponible dans leur site directement. 
Utilisation de  Jquery **tablesorter** (trié le tableau) et **autocomplete** (proposer du text lorsque utilisateur écrit un mot dans un champ spécifique)
  
*explore.html*
- **Creation de la page d'exploration** qui va contenir des liens pour acceder a:

    - **formulaire de recherche**:
        - utilisateur va pouvoir chercher les image correspondantsau microscopy

    - **Le tableau** de toute les images contenu dans **la base de donnée** et **leurs informations** comme le type d'organisme, description, microscopy ...

*listData.html* 
**Affiche les informations** en liens avec la table imagetable de la base de donnée, utilisation majoritairement de boostrap et celui ci contient une partie tablesorter réaliser via jquery

*formsearch.html* 
**Le formulaire de recherche**, celui ci contient une partie **autocomplete** réalisé via **jquery**.

*formresult.html*
**Le resultat du formulaire de recherche,** renvoie le nombre d'element trouver, l'image et le component adéquates.


# Inscription et connection 
**Création d'un environement de connection et d'inscritpion pour l'utilisateur visitant le quiz.**

### View
**importation necessaire :**
```
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .form import Registerform
from .form import Formusertable
from django.contrib.auth.models import User
from quizdjangoapp.models import usertable
```


**Fonction :**
- logoutview
    - fonction qui va permettre la deconnection et redirige vers la page d'acceuil


- register
    - Utilisation d'un formulaire pour s'enregister via Registerform.


### Url
L'inscription et la conection a besoin des urls suivante :

```
urlpatterns = [
    ...

    path('login/', views.LoginView.as_view()),
    path('home/logout/', views.LogoutView.as_view(next_page='/home/')),
    path('register/',register)

]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
```

### Forms
**importation necessaire :**
```
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import usertable
```



*Forms.py* 
Contient le **formulaire d'inscription** et les information necessaire a stocker dans la base de donnée en lien avec l'utilisateur 

La classe **Registerform** appartient a **UserCreationForm**, grace a cela on va pouvoir utiliser la table User présente dans django.

**Formusertable** utilise la table **usertable** qui permet de **stocker les informations de l'utilisateur (pseudo et score).**


### Template

*login.html* 
**Appele le formulaire pour la connection**
Si l'utilisateur n'est pas encore inscrit redirige vers la page d'inscrption

*register.html*
**Appele le formulaire pour l'inscription**

Utilisation de crispy_forms, on rajoute (importation faite en amont):
`{% load crispy_forms_tags %}`

Si l'utilisateur est deja  inscrit redirige vers la page de connection



# Dossier static
**Fichier necessaire  :**

- **Js dossier:**

    - Pour l'utilisation de bootstrap:
        - *bootstrap.min.js*
        - *bootstrap.min.js*



    - Pour l'utilisation de jQuery:
        - *jquery-3.4.1.min.js*
        - *jquery-ui.min.js*
        - *jquery.js*
        - *jquery.tablesorter.js*
        - *jquery.tablesorter.widgets.js

            
    
    - Récupere la réponse de l'utilisateur, et contient la fonction jquery du tablesorter:
        - *script.js
        



- **Css dossier :**
    
    - Pour l'utilisation de bootstrap:
        - *bootstrap.min.css*

    - Pour l'utilisation de jQuery:
        - *jquery.tablesorter.pager.css*
               
    - Css pour la page d'inscritpion et de connection    
        - *style_log.css*
    
    - Css pour le quizz, la page d'exploration et le formulaire de recherche
        - *style.css*



# Parametre a rajouter dans le setting.py

### Application definition

```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'quizdjangoapp',                            <- on rajoute l'application
    'crispy_forms'                              <- on rajoute cette ligne pour l'inscription

]
```


### Static files (CSS, JavaScript, Images)
### https://docs.djangoproject.com/en/3.1/howto/static-files/
`STATIC_URL = '/static/'`

###superuser
```
LOGIN_REDIRECT_URL='/home' #redirige vars la page helloworld quand on se login
LOGOUT_REDIRECT_URL='/login' #redirige  quand on se logout
```

# Arborescence  

```
├── db.sqlite3
├── manage.py
├── quizdjango
│   ├── __init__.py
│   ├── asgi.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── quizdjangoapp
│   ├── form.py
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   ├── 0002_questiontable.py
│   │   ├── 0003_imagetable.py
│   │   ├── 0004_usertable.py
│   │   └── __init__.py
│   ├── models.py
│   ├── static
│   │   ├── css
│   │   │   ├── bootstrap.min.css
│   │   │   ├── jquery.tablesorter.pager.css
│   │   │   ├── style_log.css
│   │   │   └── style.css
│   │   ├── image
│   │   │   ├── 10276.jpg
│   │   │   ├── ...
│   │   │   ├── 9905.jpg
│   │   │   ├── list.png
│   │   │   └── search.png
│   │   └── js
│   │       ├── bootstrap.min.js
│   │       ├── jquery-3.4.1.min.js
│   │       ├── jquery-ui.min.js
│   │       ├── jquery.js
│   │       ├── jquery.tablesorter.js
│   │       ├── jquery.tablesorter.widgets.js
│   │       ├── pagination.js
│   │       └── script.js
│   ├── templates
│   │   ├── base.html
│   │   ├── explore.html
│   │   ├── formresult.html
│   │   ├── formsearch.html
│   │   ├── home.html
│   │   ├── listData.html
│   │   ├── quiz.html
│   │   ├── registration
│   │   │   ├── login.html
│   │   │   └── register.html
│   │   ├── result.html
│   │   └── welcom.html
│   └── views.py
└── trash
```

