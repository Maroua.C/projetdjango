"""quizdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path


#DATABASE explore them to check their contents
from quizdjangoapp.views import anwser_test
from quizdjangoapp.views import question_test
from quizdjangoapp.views import image_test

#TEMPLATE HTML
from quizdjangoapp.views import welcom
from quizdjangoapp.views import quiz
from quizdjangoapp.views import result
from quizdjangoapp.views import listData
from quizdjangoapp.views import explore


#save results
from quizdjangoapp.views import saveanwser

# formulaire part into explore section
from quizdjangoapp.views import formsearch
from quizdjangoapp.views import formresult


#Register and login part
from django.conf.urls.static import static
from django.conf import settings
from quizdjangoapp.views import register
from django.contrib.auth import views 

#help page
from quizdjangoapp.views import help


urlpatterns = [
    path('admin/', admin.site.urls),

    #Database part test
    path('anwser/',anwser_test),
    path('question/',question_test),
    path('image/',image_test),

    #Welcom page
    path('home/', welcom), #directement sur url de base

    #Quiz part 
    path('quiz/<question_type>', quiz),
    #result of the quiz
    path('result/<question_type>', result),
    #saveanwser is use to save anwser given by the user
    path('saveanwser/', saveanwser),

    #Explore part
    path('listData/',listData),
    path('explore/',explore),
    #formulaire part into explore section
    path('formsearch/', formsearch),
    path('formresult/<str:mode>/', formresult),

    #Register and login part
    #Login
    path('login/', views.LoginView.as_view()),
    path('home/logout/', views.LogoutView.as_view(next_page='/home/')),
    #Register
    path('register/',register),

    #help part
    path('help/',help)

]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

