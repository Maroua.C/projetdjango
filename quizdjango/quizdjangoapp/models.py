# Create your models here.
from django.db import models
from django.contrib.auth.models import User

#Create table 

#Anwser
class anwsertable(models.Model):
    questionid = models.IntegerField(default=0)
    answer = models.CharField(max_length=255)
    definition = models.TextField()


#Question
class questiontable(models.Model):
    questionid = models.IntegerField(default=0)
    question = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    imagefield = models.CharField(max_length=255)
    point = models.IntegerField(default=0)
    n_answer = models.IntegerField(default=0)
    n_image = models.IntegerField(default=0)

#Image
#store pictures into database
class imagetable(models.Model):
    name = models.CharField(max_length=255)
    image = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    mode = models.CharField(max_length=255)
    celltype = models.CharField(max_length=255)
    component = models.CharField(max_length=255)
    doi = models.CharField(max_length=255)
    organism = models.CharField(max_length=255)


#register part
class usertable(models.Model):
    name = models.OneToOneField(User, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)

