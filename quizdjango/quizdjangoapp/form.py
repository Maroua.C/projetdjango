
from django import forms
from django.db import models


#for the search form
from .models import imagetable


#LOGIN
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import usertable



### Form search 
class Formimagetable(forms.ModelForm):

    # ModelForm have the Meta class 
    class Meta:
        model = imagetable
        fields = ['mode',] #Search part will be done with the mode column 


    


### Register part
class Registerform(UserCreationForm):
    #score = models.CharField(max_length=100)
    class Meta:
        model = User
        fields = ('username',) # no need to put 'password1', 'password2' it work without it 

### form that is use to get information frm the usertable table 
class Formusertable(forms.ModelForm):

    class Meta:
        model = usertable
        fields = ['score']
 
