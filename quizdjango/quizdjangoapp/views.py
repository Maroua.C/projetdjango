# Create your views here.

from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import redirect


#DATABASE
from quizdjangoapp.models import anwsertable
from quizdjangoapp.models import questiontable
from quizdjangoapp.models import imagetable



#import random to load picture from the database randomly
import random

#SEARCH FORM 
from .form import Formimagetable 



#LOGIN AND REGISTER
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .form import Registerform
from .form import Formusertable
from django.contrib.auth.models import User
from quizdjangoapp.models import usertable



###############################################
#_______________ Test part ___________________#
###############################################


#Test to see if table (anwsertable,questiontable and imagetable) are loaded into sqlite
def anwser_test(request,):
    return render(request,"base.html",
                               {"anwsers" : anwsertable.objects.all()})

def question_test(request,):
    return render(request,"base.html",
                               {"questions" : questiontable.objects.all()})

def image_test(request,):
    return render(request,"base.html",
                               {"images" : imagetable.objects.all()})


###############################################
#_______________Home page_____________________#
###############################################
def welcom(request):
    #get random number between 0 to 141 (because we have 141 pictures in the database)
    rand_img = random.randint(0, 141)
    print(rand_img)
    #return the imagetable.objects.all() into "image" to use image database into home page
    return render(request,"welcom.html",{"rand_img": rand_img,"images" : imagetable.objects.all()})


###############################################
#_______________ Quiz part ___________________#
###############################################

### save responce 
# reponse_microscopy save responce from microscopy questions and reponse_component is for component questions
reponse_microscopy=[] 
reponse_component=[]

### Quiz page
def quiz(request,question_type):
    # list to save pictures id this will be reuse later 
    save_id=[]

    # use to load a specific number of random anwser (the number depend on the question)
    x = random.randint(5, 8) #8 is the max number of responce
    y = x + 5

    # put id image into list
    img_x_list=[]
    # load questiontable 
    questions = questiontable.objects.all()
    
    for question in questions:
        # use to separate component and miscocopy question
        if question.type == question_type:
            #num_limit store the limit of anwser per question this info is given by question.n_image
            num_limit=question.n_image 

    # loop to store random number this will be use to load random picture 
    i=1
    while i <= num_limit:
        img_x = random.randrange(0, 141)
        img_x_list.append(img_x)
        #save_id have the same info than img_x_list but save_id will be use later and if you use img_x_list directly it won't work
        save_id.append(img_x) 
        i=i+1

    # print the id 
    print(save_id,"RANDOM NUMBER FOR ID")
    
    # load imagetable
    images = imagetable.objects.all()

    # loop into save_id
    for id in save_id:
        for image in images:  
            # get the id from (image.id)     
            if image.id == id:
                # store the microscopy and component information now we have the information for each picture 
                # (this will be compare to the anwser to check if the use have the right anwser  )
                reponse_microscopy.append(image.mode)
                reponse_component.append(image.component)
    

    return render(request, "quiz.html",
                  {"questions": questiontable.objects.all(), "anwsers" : anwsertable.objects.all() ,
                   "images" : imagetable.objects.all(), "x" : x , "y" : y,
                   "img_x_list" : img_x_list ,"question_type":question_type })

### Save the anwser 
def saveanwser(request):
    #get the anwser related to js
    anwser= request.GET['anwser']
    #check the anwser in the terminal
    print("\n\n\n",anwser,"\n\n\n")
    lst_anwsers.append(anwser)

    
### GLOBAL VARIABLE
  
#get the anwsers from radio and save it to a list
#we have to use a list otherwise it won't work
lst_anwsers=[] 


### Result quiz page 
def result(request,question_type):

    # load questiontable 
    questions = questiontable.objects.all()
    # check the responce choosen by user in ther terminal
    print("responce choosen",lst_anwsers) 
    
    # variables use to save the score 
    score = []
    #ans_right =  right awnser 
    ans_right=0
    final_score = 0

    #loop to check all responses given by the user 
    for element in lst_anwsers:
        if question_type == 'component':
            if element in reponse_component:
                #correct because reponse_component have the pictures information use into the question
                print('correct !!')
                # variable ans_right will change to 1 because the answer is correct
                ans_right = 1
            else:
                print('non correct')
            
            print(reponse_component)

        elif question_type == 'microscopy':
            #correct because reponse_microscopy have the pictures information use into the question
            if element in reponse_microscopy:
                print('correct !!')
                # variable ans_right will change to 1 because the answer is correct
                ans_right = 1
            else:
                print('non correct')
            
            print(reponse_microscopy)

    #delete all elements from lst_anwsers this will avoid keeping the past anwsers
    lst_anwsers[:] = [] 
   
    # the awnser given by the user is correct
    if ans_right == 1: # correct
        #loop in questiontable 
        for question in questions:
            if question.type == question_type:
                # store the score for the specific question in fact each question have a specific score 
                score.append(question.point)

        # sum all scores 
        for pts in score:
            final_score += pts
    else :
        # awnser is not correct 
        # no point that why we put 0
        score.append(0)
        final_score=0
    
    """
    # save the score into sqlite3 database
    userobj = usertable.objects.get(id=1)
    userobj.score = score
    userobj.save()
    """

    return render(request,"result.html",{"question_type":question_type,"score": score,"SCORE":final_score})







###############################################
#______________ Explore part ________________#
###############################################

### Explore page
def explore(request,):
    return render(request,"explore.html")

### Page which contain all data informations 
def listData(request,):
    #return the imagetable.objects.all() into "image" to use image database into listData page
    return render(request,"listData.html",
                  {"images":imagetable.objects.all()})


### form part use to search for picture using microscopy information
def formsearch(request,):
    # Formimagetable is a form (into form.py)
    form = Formimagetable(request.POST)
    if request.method == "POST":
        form = Formimagetable(request.POST)
        #form is valid thus we can save and show the result
        if form.is_valid():
            formimagetable = form.save(commit=False) 
            return redirect('/formresult/{}/'.format(formimagetable.mode))
    else:
        form = Formimagetable()
    
    return render(request, 'formsearch.html', {'form': form})


def formresult(request, mode):
    # value to see if we have a result or not 
    ifcondition=0 #pour poser la conditon dans le template

    # dictionnary keys are picture and values are component
    listinput={}

    # loops into imagetable
    for element in imagetable.objects.all():
        # mode is the value we are looking for 
        if mode in element.mode:
            #we have results
            ifcondition=1
            listinput[element.image]=element.component
            htmlif = """Result is: {}""".format(mode)
        else :
            #we don't have result
            htmlelse = """{}  no found """.format(mode)

    # the len of values into the dictionnary  this will give use the number of results found       
    len_listinput=len(listinput)-1
    return render(request, 'formresult.html', {'htmlif': htmlif,"htmlelse" : htmlelse,"ifcondition":ifcondition,
    "listinput":listinput,"len_listinput":len_listinput})


###############################################
#___________ Register and Login_______________#
###############################################


### SUPER USER PART
@login_required(login_url='/login/')

### Logout part
def logoutview(request):
    logout(request)
    # Redirect to the home page.
    return redirect("home/")

### Register part 
def register(request):
    # it's a form
    if request.method == 'POST':
        form = Registerform(request.POST)
        #the form is valid we can register save information and redirect users to the home page 
        if form.is_valid():
            form.save()
        return redirect("/home/")
    else:
        form=Registerform()
    return render(request,"registration/register.html",{"form":form})


###############################################
#________________ help page __________________#
###############################################

def help(request,):
    return render(request,"help.html")

