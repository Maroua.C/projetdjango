import csv
import os
path = "/Users/marwa/PycharmProjects/djangoprojet/source_project"  #Set path of new directory here
os.chdir(path) # changes the directory
from quizdjangoapp.models import imagetable # imports the model
with open('images.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        p = imagetable(name=row['name'], image='/image/'+row['name']+'.jpg',
                       description=row['description'], mode=row['mode'], celltype=row['celltype'],
                       component=row['component'], doi=row['doi'], organism=row['organism'])
        p.save()
